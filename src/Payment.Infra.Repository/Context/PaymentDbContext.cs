﻿using Microsoft.EntityFrameworkCore;
using Payment.Domain.Model.Models;
using System.Reflection;

namespace Payment.Infra.Repository.Context;

public class PaymentDbContext : DbContext
{
    public PaymentDbContext(DbContextOptions options)
        : base(options) { }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        base.OnModelCreating(modelBuilder);
        modelBuilder.ApplyConfigurationsFromAssembly(Assembly.GetExecutingAssembly());
    }

    public DbSet<Produto> Produtos { get; set; } = default!;

    public DbSet<ProdutoVendido> ProdutosVendidos { get; set; } = default!;

    public DbSet<Pedido> VendasRealizadas { get; set; } = default!;

    public DbSet<Vendedor> Vendedores{ get; set; } = default!;
}