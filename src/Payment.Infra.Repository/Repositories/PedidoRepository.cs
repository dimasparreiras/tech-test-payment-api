﻿using Mapster;
using Microsoft.EntityFrameworkCore;
using Payment.Application.Abstraction.Enums;
using Payment.Application.Abstraction.Interfaces.Repositories;
using Payment.Application.Abstraction.Responses;
using Payment.Application.Responses;
using Payment.Domain.Model.Models;
using Payment.Infra.Repository.Context;

namespace Payment.Infra.Repository.Repositories;

public class PedidoRepository(PaymentDbContext paymentDbContext, IProdutoVendidoRepository produtoVendidoRepository) :
    BaseRepository<Pedido>(paymentDbContext), IPedidoRepository
{
    protected readonly PaymentDbContext PaymentContext = paymentDbContext;
    protected readonly IProdutoVendidoRepository produtoVendidoRepository = produtoVendidoRepository;

    public new async Task<IEnumerable<PedidoResponse>> ListarTodosAsync()
    {
        IEnumerable<Pedido> pedidos = await Context.Set<Pedido>()
            .Include(x => x.ProdutosVendidos)
                .ThenInclude(x => x.Produto)
            .Include(x => x.Vendedor)
            .AsNoTracking()
            .ToListAsync();

        return pedidos.Adapt<IEnumerable<PedidoResponse>>();
    }

    public async Task<Pedido?> ObterModelPorIdAsync(int id)
    {
        return await PaymentContext
            .Set<Pedido>()
            .Include(x => x.ProdutosVendidos)
                .ThenInclude(x => x.Produto)
            .Include(x => x.Vendedor)
            .FirstOrDefaultAsync(p => p.Id == id);
    }

    public async Task<Response<PedidoResponse>> ObterPorIdAsync(int id)
    {
        Pedido? pedido = await ObterModelPorIdAsync(id);

        return pedido is null
            ? new(ResponseStatus.NotFound, "Não foi encontrado nenhum pedido para o ID informado.")
            : new(pedido.Adapt<PedidoResponse>());
    }

    public async Task<int> ObterProximoId()
    {
        IEnumerable<Pedido> vendaRealizada = await base.ListarTodosAsync();

        return vendaRealizada
            .Select(x => x.Id)
            .DefaultIfEmpty(0)
            .Max() + 1;
    }
}
