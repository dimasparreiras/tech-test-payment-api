﻿using Payment.Application.Abstraction.Interfaces.Repositories;
using Payment.Domain.Model.Models;
using Payment.Infra.Repository.Context;

namespace Payment.Infra.Repository.Repositories;

public class ProdutoVendidoRepository(PaymentDbContext paymentDbContext) :
    BaseRepository<ProdutoVendido>(paymentDbContext), IProdutoVendidoRepository
{
    protected readonly PaymentDbContext PaymentContext = paymentDbContext;

    public async Task<int> ObterProximoId()
    {
        IEnumerable<ProdutoVendido> produtoVendido = await base.ListarTodosAsync();

        return produtoVendido
            .Select(x => x.Id)
            .DefaultIfEmpty(0)
            .Max() + 1;
    }
}
