﻿using Mapster;
using Microsoft.EntityFrameworkCore;
using Payment.Application.Abstraction.Enums;
using Payment.Application.Abstraction.Interfaces.Repositories;
using Payment.Application.Abstraction.Responses;
using Payment.Application.Responses;
using Payment.Domain.Model.Models;
using Payment.Infra.Repository.Context;

namespace Payment.Infra.Repository.Repositories;

public class VendedorRepository(PaymentDbContext paymentDbContext) : 
    BaseRepository<Vendedor>(paymentDbContext), IVendedorRepository
{
    protected readonly PaymentDbContext PaymentContext = paymentDbContext;

    public async Task<Response<VendedorResponse>> ObterPorIdAsync(int id)
    {
        Vendedor? vendedor = await ObterModelPorIdAsync(id);

        return vendedor is null
            ? new(ResponseStatus.NotFound, "Vendedor não encontrado para o Id informado.")
            : new(vendedor.Adapt<VendedorResponse>());
    }

    public async Task<Vendedor?> ObterModelPorIdAsync(int id)
    {
        Vendedor? vendedor = await PaymentContext
            .Set<Vendedor>()
            .FirstOrDefaultAsync(p => p.Id == id);

        return vendedor;
    }

    public async Task<int> ObterProximoId()
    {
        IEnumerable<Vendedor> vendedores = await base.ListarTodosAsync();

        return vendedores
            .Select(x => x.Id)
            .DefaultIfEmpty(0)
            .Max() + 1;
    }

    public new async Task<IEnumerable<VendedorResponse>> ListarTodosAsync()
    {
        IEnumerable<Vendedor> vendedores = await base.ListarTodosAsync();

        return vendedores.Adapt<IEnumerable<VendedorResponse>>();
    }
}
