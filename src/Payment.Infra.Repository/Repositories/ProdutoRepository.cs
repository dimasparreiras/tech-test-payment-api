﻿using Mapster;
using Microsoft.EntityFrameworkCore;
using Payment.Application.Abstraction.Enums;
using Payment.Application.Abstraction.Interfaces.Repositories;
using Payment.Application.Abstraction.Responses;
using Payment.Application.Responses;
using Payment.Domain.Model.Models;
using Payment.Infra.Repository.Context;

namespace Payment.Infra.Repository.Repositories;

public class ProdutoRepository(PaymentDbContext paymentDbContext) : 
    BaseRepository<Produto>(paymentDbContext), IProdutoRepository
{
    protected readonly PaymentDbContext PaymentContext = paymentDbContext;

    public async Task<Produto?> ObterModelPorIdAsync(int id)
    {
        Produto? produto = await PaymentContext
            .Set<Produto>()
            .FirstOrDefaultAsync(p => p.Id == id);

        return produto;
    }

    public async Task<IEnumerable<Produto>> ObterModelPorIdAsync(int[] ids)
    {
        IEnumerable<Produto> produto = await PaymentContext
            .Set<Produto>()
            .Where(p => ids.Contains(p.Id))
            .ToListAsync();

        return produto;
    }

    public async Task<Response<ProdutoResponse>> ObterPorIdAsync(int id)
    {
        Produto? produto = await ObterModelPorIdAsync(id);

        return produto is null
            ? new(ResponseStatus.NotFound, "Produto não encontrado para o Id informado.")
            : new(produto.Adapt<ProdutoResponse>());
    }

    public async Task<int> ObterProximoId()
    {
        IEnumerable<Produto> produtos = await base.ListarTodosAsync();

        return produtos
            .Select(x => x.Id)
            .DefaultIfEmpty(0)
            .Max() + 1;
    }

    public new async Task<IEnumerable<ProdutoResponse>> ListarTodosAsync()
    {
        IEnumerable<Produto> produtos = await base.ListarTodosAsync();

        return produtos.Adapt<IEnumerable<ProdutoResponse>>();
    }
}
