﻿using Microsoft.EntityFrameworkCore;
using Payment.Application.Abstraction.Enums;
using Payment.Application.Abstraction.Interfaces.Repositories;
using Payment.Application.Abstraction.Responses;
using Payment.Domain.Model.Models;
using Payment.Infra.Repository.Context;

namespace Payment.Infra.Repository.Repositories;

public class BaseRepository<TEntity>(PaymentDbContext paymentDbContext) :
    IBaseRepository<TEntity> where TEntity : class
{
    protected readonly PaymentDbContext Context = paymentDbContext;

    public virtual async Task<IEnumerable<TEntity>> ListarTodosAsync()
    {
        return await Context.Set<TEntity>()
            .AsNoTracking()
            .ToListAsync();
    }

    public virtual async Task<Response> AdicionarAsync(TEntity objeto)
    {
        try
        {
            Context.Add(objeto);
            await Context.SaveChangesAsync();
            return new();
        }
        catch (Exception ex)
        {
            return new Response(ResponseStatus.InternalServerError, ex.Message);
        }
    }

    public virtual async Task<Response> AtualizarAsync(TEntity objeto)
    {
        try
        {
           Context.Entry(objeto).State = EntityState.Modified;
            await Context.SaveChangesAsync();
            return new();
        }
        catch (Exception ex)
        {
            return new Response(ResponseStatus.InternalServerError, ex.Message);
        }
    }

    public void Dispose() => Context.Dispose();
}
