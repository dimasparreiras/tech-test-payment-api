﻿using Payment.Application.Abstraction.Responses;

namespace Payment.Application.Abstraction.Interfaces.Repositories;

public interface IBaseRepository<TEntity> : IDisposable where TEntity : class
{
    /// <summary>
    /// Lista todos os registros da tabela
    /// </summary>
    /// <returns></returns>
    Task<IEnumerable<TEntity>> ListarTodosAsync();

    /// <summary>
    /// Adiciona um novo registro a tabela
    /// </summary>
    /// <param name="objeto"></param>
    /// <returns></returns>
    Task<Response> AdicionarAsync(TEntity objeto);

    /// <summary>
    /// Atualizar um registro da tabela
    /// </summary>
    /// <param name="objeto"></param>
    /// <returns></returns>
    Task<Response> AtualizarAsync(TEntity objeto);
}