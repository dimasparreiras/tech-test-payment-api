﻿using Payment.Application.Abstraction.Responses;
using Payment.Application.Responses;
using Payment.Domain.Model.Models;

namespace Payment.Application.Abstraction.Interfaces.Repositories;

public interface IVendedorRepository
{
    /// <summary>
    /// Lista todos os registros da tabela
    /// </summary>
    /// <returns></returns>
    Task<IEnumerable<VendedorResponse>> ListarTodosAsync();

    /// <summary>
    /// Adiciona um novo registro a tabela
    /// </summary>
    /// <param name="objeto"></param>
    /// <returns></returns>
    Task<Response> AdicionarAsync(Vendedor vendedor);

    /// <summary>
    /// Atualizar um registro da tabela
    /// </summary>
    /// <param name="objeto"></param>
    /// <returns></returns>
    Task<Response> AtualizarAsync(Vendedor vendedor);

    /// <summary>
    /// Busca um registro pelo ID
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    Task<Response<VendedorResponse>> ObterPorIdAsync(int id);

    /// <summary>
    /// Busca um registro pelo ID
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    Task<Vendedor?> ObterModelPorIdAsync(int id);

    /// <summary>
    /// Obter o próximo ID
    /// </summary>
    /// <returns></returns>
    Task<int> ObterProximoId();
}
