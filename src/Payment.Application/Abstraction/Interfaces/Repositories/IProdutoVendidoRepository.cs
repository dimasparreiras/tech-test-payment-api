﻿using Payment.Application.Abstraction.Responses;
using Payment.Domain.Model.Models;

namespace Payment.Application.Abstraction.Interfaces.Repositories;

public interface IProdutoVendidoRepository
{
    /// <summary>
    /// Obter o próximo ID
    /// </summary>
    /// <returns></returns>
    Task<int> ObterProximoId();
}
