﻿using Payment.Application.Abstraction.Responses;
using Payment.Application.Responses;
using Payment.Domain.Model.Models;

namespace Payment.Application.Abstraction.Interfaces.Repositories;

public interface IProdutoRepository
{
    /// <summary>
    /// Lista todos os registros da tabela
    /// </summary>
    /// <returns></returns>
    Task<IEnumerable<ProdutoResponse>> ListarTodosAsync();

    /// <summary>
    /// Adiciona um novo registro a tabela
    /// </summary>
    /// <param name="objeto"></param>
    /// <returns></returns>
    Task<Response> AdicionarAsync(Produto objeto);

    /// <summary>
    /// Atualizar um registro da tabela
    /// </summary>
    /// <param name="objeto"></param>
    /// <returns></returns>
    Task<Response> AtualizarAsync(Produto objeto);

    /// <summary>
    /// Busca um registro pelo ID
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    Task<Response<ProdutoResponse>> ObterPorIdAsync(int id);

    /// <summary>
    /// Busca um registro pelo ID
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    Task<Produto?> ObterModelPorIdAsync(int id);

    /// <summary>
    /// Busca um registro pelo ID
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    Task<IEnumerable<Produto>> ObterModelPorIdAsync(int[] ids);

    /// <summary>
    /// Obter o próximo ID
    /// </summary>
    /// <returns></returns>
    Task<int> ObterProximoId();
}
