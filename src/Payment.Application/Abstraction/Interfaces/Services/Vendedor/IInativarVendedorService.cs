﻿using Payment.Application.Abstraction.Responses;

namespace Payment.Application.Abstraction.Interfaces.Services.Vendedor
{
    public interface IInativarVendedorService
    {
        Task<Response> Execute(int id);
    }
}
