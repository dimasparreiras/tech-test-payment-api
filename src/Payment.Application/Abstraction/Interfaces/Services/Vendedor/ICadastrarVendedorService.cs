﻿using Payment.Application.Abstraction.Responses;
using Payment.Application.Requests;

namespace Payment.Application.Abstraction.Interfaces.Services.Vendedor
{
    public interface ICadastrarVendedorService
    {
        Task<Response> Execute(CadastrarVendedorRequest request);
    }
}
