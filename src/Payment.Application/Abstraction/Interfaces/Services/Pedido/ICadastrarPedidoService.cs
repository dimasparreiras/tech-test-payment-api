﻿using Payment.Application.Abstraction.Responses;
using Payment.Application.Requests;

namespace Payment.Application.Abstraction.Interfaces.Services.Venda;

public interface ICadastrarPedidoService
{
    Task<Response> Execute(CadastrarPedidoRequest request);
}
