﻿using Payment.Application.Abstraction.Responses;

namespace Payment.Application.Abstraction.Interfaces.Services.Pedido;

public interface IInformarEntregaService
{
    Task<Response> Execute(int request);
}
