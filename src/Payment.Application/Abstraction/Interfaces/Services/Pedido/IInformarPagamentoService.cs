﻿using Payment.Application.Abstraction.Responses;

namespace Payment.Application.Abstraction.Interfaces.Services.Pedido;

public interface IInformarPagamentoService
{
    Task<Response> Execute(int request);
}
