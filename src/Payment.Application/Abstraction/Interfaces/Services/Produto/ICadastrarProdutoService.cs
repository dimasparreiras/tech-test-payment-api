﻿using Payment.Application.Abstraction.Responses;
using Payment.Application.Requests;

namespace Payment.Application.Abstraction.Interfaces.Services.Produto
{
    public interface ICadastrarProdutoService
    {
        Task<Response> Execute(CadastrarProdutoRequest request);
    }
}
