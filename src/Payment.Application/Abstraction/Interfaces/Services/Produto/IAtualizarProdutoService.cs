﻿using Payment.Application.Abstraction.Responses;
using Payment.Application.Requests;

namespace Payment.Application.Abstraction.Interfaces.Services.Produto
{
    public interface IAtualizarProdutoService
    {
        Task<Response> Execute(AtualizarProdutoRequest request);
    }
}