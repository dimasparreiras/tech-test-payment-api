﻿using Payment.Application.Abstraction.Responses;

namespace Payment.Application.Abstraction.Interfaces.Services.Produto
{
    public interface IInativarProdutoService
    {
        Task<Response> Execute(int id);
    }
}
