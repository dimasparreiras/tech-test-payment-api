﻿using Payment.Application.Abstraction.Enums;
using System.Text.Json.Serialization;

namespace Payment.Application.Abstraction.Responses;

public class Response
{
    public Response()
    {

    }

    public Response(object result)
        : this()
    {
        Result = result;
    }

    public Response(Exception exception)
        : this()
    {
        Status = ResponseStatus.InternalServerError;
        Message = exception.Message;
    }

    public Response(ResponseStatus responseStatus, string message)
        : this()
    {
        Status = responseStatus;
        Message = message;
    }

    public ResponseStatus Status { get; set; } = ResponseStatus.Ok;

    public string Message { get; set; }

    [JsonIgnore]
    public object Result { get; set; }

    [JsonIgnore]
    public bool IsSuccess => Status == ResponseStatus.Ok;
}

public class Response<TResult> : Response
{
    public Response() { }

    public Response(TResult result) : base(result) { }

    public Response(Exception exception) : base(exception) { }

    public Response(ResponseStatus responseStatus, string message) : base(responseStatus, message) { }

    public new TResult Result
    {
        get => (TResult)base.Result;
        set => base.Result = value;
    }
}
