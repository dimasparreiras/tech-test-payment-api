﻿using Mapster;
using Payment.Application.Abstraction.Extensions;
using Payment.Application.Responses;
using Payment.Domain.Model.Enums;
using Payment.Domain.Model.Models;

namespace Payment.Application.Mappings
{
    internal class PedidoMapping
        : IRegister
    {
        public void Register(TypeAdapterConfig config)
        {
            config.NewConfig<Pedido, PedidoResponse>()
                .Map(dest => dest.Id, src => src.Id)
                .Map(dest => dest.Vendedor, src => src.Vendedor.Adapt<VendedorResponse>())
                .Map(dest => dest.DataVenda, src => src.DataVenda)
                .Map(dest => dest.Status, src => ((StatusPedido)src.Status).GetEnumDescription())
                .Map(dest => dest.ProdutosVendidos, src => MapearProdutosVendidos(src.ProdutosVendidos));
        }

        private IEnumerable<ProdutoVendidoResponse> MapearProdutosVendidos(
            IEnumerable<ProdutoVendido> produtosVendidos)
        {
            return produtosVendidos.Select(p =>
            {
                return new ProdutoVendidoResponse()
                {
                    DescricaoProduto = p.Produto.Descricao,
                    Quantidade = p.Quantidade,
                    Valor = p.Quantidade * p.Produto.Valor
                };
            });
        }
    }
}
