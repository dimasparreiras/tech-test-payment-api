﻿using System.ComponentModel.DataAnnotations;

namespace Payment.Application.Requests
{
    public class CadastrarPedidoRequest
    {
        [Required(ErrorMessage = "Informe o Id do vendedor")]
        public int IdVendedor { get; set; }

        [Required(ErrorMessage = "Informe os produtos")]
        public IEnumerable<ProdutoVendidoRequest> Produtos { get; set; }
    }
}
