﻿using System.ComponentModel.DataAnnotations;

namespace Payment.Application.Requests
{
    public class ProdutoVendidoRequest
    {
        [Required(ErrorMessage = "Informe o Id do produto")]
        public int IdProduto { get; set; }

        [Required(ErrorMessage = "Informe a quantidade do produto")]
        public int Quantidade { get; set; }
    }
}
