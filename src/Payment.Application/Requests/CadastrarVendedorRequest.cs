﻿using System.ComponentModel.DataAnnotations;
namespace Payment.Application.Requests
{
    public class CadastrarVendedorRequest
    {
        [Required(ErrorMessage = "Informe o nome do vendedor")]
        public string Nome { get; set; }

        [Required(ErrorMessage = "Informe o CPF do vendedor")]
        public string Cpf { get; set; }

        [Required(ErrorMessage = "Informe o e-mail do vendedor")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Informe o telefone do vendedor")]
        public string Telefone { get; set; }
    }
}
