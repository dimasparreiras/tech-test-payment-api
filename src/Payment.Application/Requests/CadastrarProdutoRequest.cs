﻿using System.ComponentModel.DataAnnotations;

namespace Payment.Application.Requests
{
    public class CadastrarProdutoRequest
    {
        [Required(ErrorMessage = "Informe a Descrição do produto")]
        public string Descricao { get; set; }

        [Required(ErrorMessage = "Informe o Valor do produto")]
        public decimal Valor { get; set; }
    }
}
