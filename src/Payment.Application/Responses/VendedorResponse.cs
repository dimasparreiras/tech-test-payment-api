﻿namespace Payment.Application.Responses
{
    public class VendedorResponse
    {
        public VendedorResponse()
        {
        }

        public VendedorResponse(
            int id,
            string nome,
            string cpf,
            string email,
            string telefone,
            bool ativo)
        {
            Id = id;
            Nome = nome;
            Cpf = cpf;
            Email = email;
            Telefone = telefone;
            Ativo = ativo;
        }

        public int Id { get; set; }

        public string Nome { get; set; }

        public string Cpf { get; set; }

        public string Email { get; set; }

        public string Telefone { get; set; }

        public bool Ativo { get; set; }
    }
}
