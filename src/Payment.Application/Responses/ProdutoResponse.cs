﻿namespace Payment.Application.Responses
{
    public class ProdutoResponse
    {
        public ProdutoResponse()
        {
        }

        public ProdutoResponse(
            int id,
            string descricao,
            decimal valor,
            bool ativo)
        {
            Id = id;
            Descricao = descricao;
            Valor = valor;
            Ativo = ativo;
        }

        public int Id { get; set; }

        public string Descricao { get; set; }

        public decimal Valor { get; set; }

        public bool Ativo { get; set; }
    }
}
