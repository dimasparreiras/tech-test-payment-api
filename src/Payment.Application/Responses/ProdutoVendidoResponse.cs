﻿namespace Payment.Application.Responses
{
    public class ProdutoVendidoResponse
    {
        public string DescricaoProduto { get; set; }

        public int Quantidade { get; set; }

        public decimal Valor { get; set; }
    }
}
