﻿namespace Payment.Application.Responses
{
    public class PedidoResponse
    {
        public int Id { get; protected set; }

        public VendedorResponse Vendedor { get; set; }

        public DateTime DataVenda { get; set; }

        public string Status { get; set; }

        public IEnumerable<ProdutoVendidoResponse> ProdutosVendidos { get; set; } = [];

        public decimal TotalVendido
        {
            get => ProdutosVendidos.Sum(x => x.Quantidade * x.Valor);
        }
    }
}
