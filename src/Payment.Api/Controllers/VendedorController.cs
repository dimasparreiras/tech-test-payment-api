﻿using Microsoft.AspNetCore.Mvc;
using Payment.Application.Abstraction.Enums;
using Payment.Application.Abstraction.Interfaces.Repositories;
using Payment.Application.Abstraction.Interfaces.Services.Vendedor;
using Payment.Application.Abstraction.Responses;
using Payment.Application.Requests;
using Payment.Application.Responses;

namespace Payment.Api.Controllers;

[Route("vendedor")]
[ApiController]
public class VendedorController
    : ControllerBase
{
    private readonly IVendedorRepository vendedorRepository;
    private readonly ICadastrarVendedorService cadastrarVendedorService;
    private readonly IAtualizarVendedorService atualizarVendedorService;
    private readonly IInativarVendedorService inativarVendedorService;

    public VendedorController(
        IVendedorRepository vendedorRepository,
        ICadastrarVendedorService cadastrarVendedorService,
        IAtualizarVendedorService atualizarVendedorService,
        IInativarVendedorService inativarVendedorService)
    {
        this.vendedorRepository = vendedorRepository;
        this.cadastrarVendedorService = cadastrarVendedorService;
        this.atualizarVendedorService = atualizarVendedorService;
        this.inativarVendedorService = inativarVendedorService;
    }

    //<summary>
    // Cadastra um novo vendedor
    //</summary>
    [HttpPost]
    [ProducesResponseType(typeof(Response), (int)ResponseStatus.Ok)]
    [ProducesResponseType(typeof(Response), (int)ResponseStatus.InternalServerError)]
    public async Task<IActionResult> CadastrarVendedor(
        [FromBody] CadastrarVendedorRequest request)
    {
        return Ok(await cadastrarVendedorService.Execute(request));
    }

    /// <summary>
    /// Lista os vendedores cadastrados
    /// </summary>
    /// <param name="request">Request</param>
    /// <returns></returns>
    [HttpGet]
    [ProducesResponseType(typeof(IEnumerable<VendedorResponse>), (int)ResponseStatus.Ok)]
    [ProducesResponseType(typeof(Response), (int)ResponseStatus.NotFound)]
    public async Task<IActionResult> ListarVendedores()
    {
        return Ok(await vendedorRepository.ListarTodosAsync());
    }

    /// <summary>
    /// Consulta um vendedor pelo ID
    /// </summary>
    /// <param name="request">Request</param>
    /// <returns></returns>
    [HttpGet("{id}")]
    [ProducesResponseType(typeof(Response<VendedorResponse>), (int)ResponseStatus.Ok)]
    [ProducesResponseType(typeof(Response), (int)ResponseStatus.NotFound)]
    public async Task<IActionResult> ObterVendedor(
        [FromRoute] int id)
    {
        return Ok(await vendedorRepository.ObterPorIdAsync(id));
    }

    //<summary>
    // Atualizar um vendedor pelo ID
    //</summary>
    [HttpPut]
    [ProducesResponseType(typeof(Response), (int)ResponseStatus.Ok)]
    [ProducesResponseType(typeof(Response), (int)ResponseStatus.NotFound)]
    [ProducesResponseType(typeof(Response), (int)ResponseStatus.InternalServerError)]
    public async Task<IActionResult> AtualizarVendedor(
        [FromBody] AtualizarVendedorRequest request)
    {
        return Ok(await atualizarVendedorService.Execute(request));
    }

    //<summary>
    // Inativa um vendedor pelo ID
    //</summary>
    [HttpPut("inativar/{id}")]
    [ProducesResponseType(typeof(Response), (int)ResponseStatus.Ok)]
    [ProducesResponseType(typeof(Response), (int)ResponseStatus.NotFound)]
    [ProducesResponseType(typeof(Response), (int)ResponseStatus.InternalServerError)]
    public async Task<IActionResult> InativartVendedor(
        [FromRoute] int id)
    {
        return Ok(await inativarVendedorService.Execute(id));
    }
}
