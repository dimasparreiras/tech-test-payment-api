﻿using Microsoft.AspNetCore.Mvc;
using Payment.Application.Abstraction.Enums;
using Payment.Application.Abstraction.Interfaces.Repositories;
using Payment.Application.Abstraction.Interfaces.Services.Produto;
using Payment.Application.Abstraction.Responses;
using Payment.Application.Requests;
using Payment.Application.Responses;

namespace Payment.Api.Controllers;

[Route("produto")]
[ApiController]
public class ProdutoController
    : ControllerBase
{
    private readonly IProdutoRepository produtoRepository;
    private readonly ICadastrarProdutoService cadastrarProdutoService;
    private readonly IAtualizarProdutoService atualizarProdutoService;
    private readonly IInativarProdutoService inativarProdutoService;

    public ProdutoController(
        IProdutoRepository produtoRepository,
        ICadastrarProdutoService cadastrarProdutoService,
        IAtualizarProdutoService atualizarProdutoService,
        IInativarProdutoService inativarProdutoService)
    {
        this.produtoRepository = produtoRepository;
        this.cadastrarProdutoService = cadastrarProdutoService;
        this.atualizarProdutoService = atualizarProdutoService;
        this.inativarProdutoService = inativarProdutoService;
    }

    //<summary>
    // Cadastra um novo produto
    //</summary>
    [HttpPost]
    [ProducesResponseType(typeof(Response), (int)ResponseStatus.Ok)]
    [ProducesResponseType(typeof(Response), (int)ResponseStatus.InternalServerError)]
    public async Task<IActionResult> CadastrarProduto(
        [FromBody] CadastrarProdutoRequest request)
    {
        return Ok(await cadastrarProdutoService.Execute(request));
    }

    /// <summary>
    /// Lista os produtos cadastrados
    /// </summary>
    /// <param name="request">Request</param>
    /// <returns></returns>
    [HttpGet]
    [ProducesResponseType(typeof(IEnumerable<ProdutoResponse>), (int)ResponseStatus.Ok)]
    [ProducesResponseType(typeof(Response), (int)ResponseStatus.NotFound)]
    public async Task<IActionResult> ListarProdutos()
    {
        return Ok(await produtoRepository.ListarTodosAsync());
    }

    /// <summary>
    /// Consultar um produto pelo Id
    /// </summary>
    /// <param name="request">Request</param>
    /// <returns></returns>
    [HttpGet("{id}")]
    [ProducesResponseType(typeof(Response<ProdutoResponse>), (int)ResponseStatus.Ok)]
    [ProducesResponseType(typeof(Response), (int)ResponseStatus.NotFound)]
    public async Task<IActionResult> ObterProduto(
        [FromRoute] int id)
    {
        return Ok(await produtoRepository.ObterPorIdAsync(id));
    }

    //<summary>
    // Atualizar um produto pelo ID
    //</summary>
    [HttpPut]
    [ProducesResponseType(typeof(Response), (int)ResponseStatus.Ok)]
    [ProducesResponseType(typeof(Response), (int)ResponseStatus.NotFound)]
    [ProducesResponseType(typeof(Response), (int)ResponseStatus.InternalServerError)]
    public async Task<IActionResult> AtualizarProduto(
        [FromBody] AtualizarProdutoRequest request)
    {
        return Ok(await atualizarProdutoService.Execute(request));
    }

    //<summary>
    // Inativa um produto pelo ID
    //</summary>
    [HttpPut("inativar/{id}")]
    [ProducesResponseType(typeof(Response), (int)ResponseStatus.Ok)]
    [ProducesResponseType(typeof(Response), (int)ResponseStatus.NotFound)]
    [ProducesResponseType(typeof(Response), (int)ResponseStatus.InternalServerError)]
    public async Task<IActionResult> InativartProduto(
        [FromRoute] int id)
    {
        return Ok(await inativarProdutoService.Execute(id));
    }
}
