using Microsoft.AspNetCore.Mvc;
using Payment.Application.Abstraction.Enums;
using Payment.Application.Abstraction.Interfaces.Repositories;
using Payment.Application.Abstraction.Interfaces.Services.Pedido;
using Payment.Application.Abstraction.Interfaces.Services.Venda;
using Payment.Application.Abstraction.Responses;
using Payment.Application.Requests;
using Payment.Application.Responses;

namespace Payment.Api.Controllers;

[Route("pedido")]
[ApiController]
public class PedidoController
    : ControllerBase
{
    private readonly IPedidoRepository pedidoRepository;
    private readonly ICadastrarPedidoService cadastrarPedidoService;
    private readonly IInformarCancelamentoService informarCancelamentoService;
    private readonly IInformarEntregaService informarEntregaService;
    private readonly IInformarEnvioService informarEnvioService;
    private readonly IInformarPagamentoService informarPagamentoService;


    public PedidoController(
        IPedidoRepository pedidoRepository,
        ICadastrarPedidoService cadastrarPedidoService,
        IInformarCancelamentoService informarCancelamentoService,
        IInformarEntregaService informarEntregaService,
        IInformarEnvioService informarEnvioService,
        IInformarPagamentoService informarPagamentoService)
    {
        this.pedidoRepository = pedidoRepository;
        this.cadastrarPedidoService = cadastrarPedidoService;
        this.informarCancelamentoService = informarCancelamentoService;
        this.informarEntregaService = informarEntregaService;
        this.informarEnvioService = informarEnvioService;
        this.informarPagamentoService = informarPagamentoService;
    }

    //<summary>
    // Cadastra um novo pedido
    //</summary>
    [HttpPost]
    [ProducesResponseType(typeof(Response), (int)ResponseStatus.Ok)]
    [ProducesResponseType(typeof(Response), (int)ResponseStatus.InternalServerError)]
    public async Task<IActionResult> CadastrarPedido(
        [FromBody] CadastrarPedidoRequest request)
    {
        return Ok(await cadastrarPedidoService.Execute(request));
    }

    /// <summary>
    /// Lista os pedidos cadastrados
    /// </summary>
    /// <param name="request">Request</param>
    /// <returns></returns>
    [HttpGet]
    [ProducesResponseType(typeof(IEnumerable<PedidoResponse>), (int)ResponseStatus.Ok)]
    [ProducesResponseType(typeof(Response), (int)ResponseStatus.NotFound)]
    public async Task<IActionResult> ListarPedidos()
    {
        return Ok(await pedidoRepository.ListarTodosAsync());
    }

    /// <summary>
    /// Consulta um pedido pelo ID
    /// </summary>
    /// <param name="request">Request</param>
    /// <returns></returns>
    [HttpGet("{id}")]
    [ProducesResponseType(typeof(Response<PedidoResponse>), (int)ResponseStatus.Ok)]
    [ProducesResponseType(typeof(Response), (int)ResponseStatus.NotFound)]
    public async Task<IActionResult> ObterPedido(
        [FromRoute] int id)
    {
        return Ok(await pedidoRepository.ObterPorIdAsync(id));
    }

    ///<summary>
    /// Atualiza o status para Pagamento Aprovado
    ///</summary>
    /// <param name="id"></param>
    /// <returns></returns>
    [HttpPut("informar-pagamento/{id}")]
    [ProducesResponseType(typeof(Response), (int)ResponseStatus.Ok)]
    [ProducesResponseType(typeof(Response), (int)ResponseStatus.NotFound)]
    [ProducesResponseType(typeof(Response), (int)ResponseStatus.InternalServerError)]
    public async Task<IActionResult> InformarPagamento(
        [FromRoute] int id)
    {
        return Ok(await informarPagamentoService.Execute(id));
    }

    /// <summary>
    /// Atualiza o status para Enviado para Transportadora
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    [HttpPut("informar-envio/{id}")]
    [ProducesResponseType(typeof(Response), (int)ResponseStatus.Ok)]
    [ProducesResponseType(typeof(Response), (int)ResponseStatus.NotFound)]
    [ProducesResponseType(typeof(Response), (int)ResponseStatus.InternalServerError)]
    public async Task<IActionResult> InformarEnvio(
        [FromRoute] int id)
    {
        return Ok(await informarEnvioService.Execute(id));
    }

    ///<summary>
    /// Atualiza o status para Entregue
    ///</summary>
    /// <param name="id"></param>
    /// <returns></returns>
    [HttpPut("informar-entrega/{id}")]
    [ProducesResponseType(typeof(Response), (int)ResponseStatus.Ok)]
    [ProducesResponseType(typeof(Response), (int)ResponseStatus.NotFound)]
    [ProducesResponseType(typeof(Response), (int)ResponseStatus.InternalServerError)]
    public async Task<IActionResult> InformarEntrega(
        [FromRoute] int id)
    {
        return Ok(await informarEntregaService.Execute(id));
    }

    ///<summary>
    /// Atualiza o status para Cancelado
    ///</summary>
    /// <param name="id"></param>
    /// <returns></returns>
    [HttpPut("informar-cancelamento/{id}")]
    [ProducesResponseType(typeof(Response), (int)ResponseStatus.Ok)]
    [ProducesResponseType(typeof(Response), (int)ResponseStatus.NotFound)]
    [ProducesResponseType(typeof(Response), (int)ResponseStatus.InternalServerError)]
    public async Task<IActionResult> InformarCancelamento(
        [FromRoute] int id)
    {
        return Ok(await informarCancelamentoService.Execute(id));
    }
}
