using Mapster;
using MapsterMapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.OpenApi.Models;
using Payment.Application.Abstraction.Interfaces.Repositories;
using Payment.Application.Abstraction.Interfaces.Services.Pedido;
using Payment.Application.Abstraction.Interfaces.Services.Produto;
using Payment.Application.Abstraction.Interfaces.Services.Venda;
using Payment.Application.Abstraction.Interfaces.Services.Vendedor;
using Payment.Domain.Model.Models;
using Payment.Domain.Services.Pedidos;
using Payment.Domain.Services.Produtos;
using Payment.Domain.Services.Vendedores;
using Payment.Infra.Repository.Context;
using Payment.Infra.Repository.Repositories;
using System.Reflection;

var builder = WebApplication.CreateBuilder(args);
string connectionString = builder.Configuration.GetConnectionString("CONNECTION_STRING") ?? string.Empty;

builder.Services.AddControllers();
builder.Services.AddEndpointsApiExplorer();

builder.Services.AddSwaggerGen(options =>
{
    options.SwaggerDoc
    (
        "v1",
        new OpenApiInfo
        {
            Title = "PaymentApi",
            Version = "0.1",
            Description = "Tech Test - Payment Api",
            Contact = new OpenApiContact
            {
                Name = "Pottencial",
                Url = new Uri(Environment.GetEnvironmentVariable("POTTENCIAL_URL") ?? string.Empty)
            }
        }
    );
});

TypeAdapterConfig typeAdapterConfig = TypeAdapterConfig.GlobalSettings;
    typeAdapterConfig.Scan(Assembly.GetExecutingAssembly());
Mapper mapperConfig = new(typeAdapterConfig);
builder.Services.AddSingleton<IMapper>(mapperConfig);

builder.Services.AddDbContext<PaymentDbContext>(options =>
{
    options.UseSqlite(connectionString, o => o.MigrationsAssembly("Payment.Api"));
});

// Injeções de Dependência

// Repositórios
builder.Services.AddScoped<IProdutoRepository, ProdutoRepository>();
builder.Services.AddScoped<IProdutoVendidoRepository, ProdutoVendidoRepository>();
builder.Services.AddScoped<IPedidoRepository, PedidoRepository>();
builder.Services.AddScoped<IVendedorRepository, VendedorRepository>();

// Services
builder.Services.AddScoped<ICadastrarVendedorService, CadastrarVendedorService>();
builder.Services.AddScoped<IAtualizarVendedorService, AtualizarVendedorService>();
builder.Services.AddScoped<IInativarVendedorService, InativarVendedorService>();

builder.Services.AddScoped<ICadastrarProdutoService, CadastrarProdutoService>();
builder.Services.AddScoped<IAtualizarProdutoService, AtualizarProdutoService>();
builder.Services.AddScoped<IInativarProdutoService, InativarProdutoService>();

builder.Services.AddScoped<ICadastrarPedidoService, CadastrarPedidoService>();
builder.Services.AddScoped<IInformarPagamentoService, InformarPagamentoService>();
builder.Services.AddScoped<IInformarEnvioService, InformarEnvioService>();
builder.Services.AddScoped<IInformarEntregaService, InformarEntregaService>();
builder.Services.AddScoped<IInformarCancelamentoService, InformarCancelamentoService>();

var app = builder.Build();

if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
