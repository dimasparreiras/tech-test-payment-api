﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Payment.Api.Migrations
{
    /// <inheritdoc />
    public partial class AtualizarTabelaPedido : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ProdutosVendidos_VendasRealizadas_VendaRealizadaId",
                table: "ProdutosVendidos");

            migrationBuilder.RenameColumn(
                name: "VendaRealizadaId",
                table: "ProdutosVendidos",
                newName: "PedidoId");

            migrationBuilder.RenameIndex(
                name: "IX_ProdutosVendidos_VendaRealizadaId",
                table: "ProdutosVendidos",
                newName: "IX_ProdutosVendidos_PedidoId");

            migrationBuilder.AddColumn<int>(
                name: "Status",
                table: "VendasRealizadas",
                type: "INTEGER",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddForeignKey(
                name: "FK_ProdutosVendidos_VendasRealizadas_PedidoId",
                table: "ProdutosVendidos",
                column: "PedidoId",
                principalTable: "VendasRealizadas",
                principalColumn: "Id");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ProdutosVendidos_VendasRealizadas_PedidoId",
                table: "ProdutosVendidos");

            migrationBuilder.DropColumn(
                name: "Status",
                table: "VendasRealizadas");

            migrationBuilder.RenameColumn(
                name: "PedidoId",
                table: "ProdutosVendidos",
                newName: "VendaRealizadaId");

            migrationBuilder.RenameIndex(
                name: "IX_ProdutosVendidos_PedidoId",
                table: "ProdutosVendidos",
                newName: "IX_ProdutosVendidos_VendaRealizadaId");

            migrationBuilder.AddForeignKey(
                name: "FK_ProdutosVendidos_VendasRealizadas_VendaRealizadaId",
                table: "ProdutosVendidos",
                column: "VendaRealizadaId",
                principalTable: "VendasRealizadas",
                principalColumn: "Id");
        }
    }
}
