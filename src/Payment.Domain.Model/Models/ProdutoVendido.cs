﻿using System.ComponentModel.DataAnnotations;

namespace Payment.Domain.Model.Models;

public class ProdutoVendido
{
    protected ProdutoVendido()
    {
        
    }

    public ProdutoVendido(
        int id,
        Produto produto,
        int quantidade)
    {
        Id = id;
        Produto = produto;
        Quantidade = quantidade;
    }

    [Key]
    public int Id { get; protected set; }

    [Required]
    public Produto Produto { get; set; }

    [Required]
    public int Quantidade { get; set; }
}
