﻿using System.ComponentModel.DataAnnotations;

namespace Payment.Domain.Model.Models;

public class Produto
{
    protected Produto()
    {
        ProdutosVendidos = [];
    }

    public Produto(
        int id,
        string descricao,
        decimal valor)
    {
        Id = id;
        Descricao = descricao;
        Valor = valor;
        Ativo = true;
    }

    [Key]
    public int Id { get; protected set; }

    [Required]
    public string Descricao { get; set; }

    [Required]
    public decimal Valor { get; set; }

    [Required]
    public bool Ativo { get; set; }

    public ICollection<ProdutoVendido> ProdutosVendidos { get; set; } = [];


    public void Atualizar(
        string descricao,
        decimal valor)
    {
        Descricao = descricao;
        Valor = valor;
    }

    public void Inativar()
    {
        Ativo = false;
    }
}
