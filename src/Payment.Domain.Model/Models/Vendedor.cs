﻿using System.ComponentModel.DataAnnotations;

namespace Payment.Domain.Model.Models;

public class Vendedor
{
    protected Vendedor()
    {
        
    }

    public Vendedor(
        int id,
        string nome,
        string cpf,
        string email,
        string telefone)
    {
        Id = id;
        Nome = nome;
        Cpf = cpf;
        Email = email;
        Telefone = telefone;
        Ativo = true;
    }

    [Key]
    public int Id { get; protected set; }

    [Required]
    public string Nome { get; set; }

    [Required]
    public string Cpf { get; set; }

    [Required]
    public string Email { get; set; }

    [Required]
    public string Telefone { get; set; }

    [Required]
    public bool Ativo {  get; set; }

    public ICollection<Pedido> VendasRealizadas { get; set; } = [];

    public void Atualizar(
        string nome,
        string cpf,
        string email,
        string telefone)
    {
        Nome = nome;
        Cpf = cpf;
        Email = email;
        Telefone = telefone;
    }

    public void Inativar()
    {
        Ativo = false;
    }
}
