﻿using Payment.Domain.Model.Enums;
using System.ComponentModel.DataAnnotations;

namespace Payment.Domain.Model.Models;

public class Pedido
{
    protected Pedido()
    {
        ProdutosVendidos = [];
    }
    public Pedido(
        int id,
        Vendedor vendedor,
        ICollection<ProdutoVendido> produtosVendidos)
    {
        Id = id;
        DataVenda = DateTime.UtcNow;
        Status = (int)StatusPedido.AguardandoPagamento;
        Vendedor = vendedor;
        ProdutosVendidos = produtosVendidos;
    }

    [Key]
    public int Id { get; set; }

    [Required]
    public Vendedor Vendedor { get; set; }

    [Required]
    [DataType(DataType.DateTime)]
    public DateTime DataVenda { get; set; }

    [Required]
    public int Status { get; set; }

    [Required]
    public ICollection<ProdutoVendido> ProdutosVendidos { get; set; }

    public decimal TotalVendido
    {
        get => ProdutosVendidos.Sum(x => x.Quantidade * x.Produto.Valor);
    }

    public void AtualizarStatus(
        StatusPedido status)
    {
        Status = (int)status;
    }
}
