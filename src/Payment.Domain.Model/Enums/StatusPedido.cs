﻿using System.ComponentModel;

namespace Payment.Domain.Model.Enums;

public enum StatusPedido
{
    [Description("Aguardando pagamento")]
    AguardandoPagamento = 1,

    [Description("Pagamento aprovado")]
    PagamentoAprovado = 2,

    [Description("Enviado para transportadora")]
    EnviadoTransportadora = 3,

    [Description("Entregue")]
    Entregue = 4,

    [Description("Cancelado")]
    Cancelado = 5
}
