﻿using Payment.Application.Abstraction.Enums;
using Payment.Application.Abstraction.Interfaces.Repositories;
using Payment.Application.Abstraction.Interfaces.Services.Vendedor;
using Payment.Application.Abstraction.Responses;
using Payment.Domain.Model.Models;

namespace Payment.Domain.Services.Vendedores;

public class InativarVendedorService : IInativarVendedorService
{
    private readonly IVendedorRepository vendedorRepository;

    public InativarVendedorService(
        IVendedorRepository vendedorRepository)
    {
        this.vendedorRepository = vendedorRepository;
    }

    public async Task<Response> Execute(
        int request)
    {
        Vendedor? vendedor = await vendedorRepository.ObterModelPorIdAsync(request);

        if (vendedor is null)
        {
            return new(ResponseStatus.NotFound, "Vendedor não encontrado para o Id informado.");
        }

        vendedor.Inativar();

        return await vendedorRepository.AtualizarAsync(vendedor);
    }
}
