﻿using Payment.Application.Abstraction.Interfaces.Repositories;
using Payment.Application.Abstraction.Interfaces.Services.Vendedor;
using Payment.Application.Abstraction.Responses;
using Payment.Application.Requests;
using Payment.Domain.Model.Models;

namespace Payment.Domain.Services.Vendedores;

public class CadastrarVendedorService : ICadastrarVendedorService
{
    private readonly IVendedorRepository vendedorRepository;

    public CadastrarVendedorService(
        IVendedorRepository vendedorRepository)
    {
        this.vendedorRepository = vendedorRepository;
    }

    public async Task<Response> Execute(
        CadastrarVendedorRequest request)
    {
        int id = await vendedorRepository.ObterProximoId();

        Vendedor vendedor = new Vendedor(
            id,
            request.Nome,
            request.Cpf,
            request.Email,
            request.Telefone);

       return await vendedorRepository.AdicionarAsync(vendedor);
    }
}
