﻿using Payment.Application.Abstraction.Enums;
using Payment.Application.Abstraction.Interfaces.Repositories;
using Payment.Application.Abstraction.Interfaces.Services.Vendedor;
using Payment.Application.Abstraction.Responses;
using Payment.Application.Requests;
using Payment.Domain.Model.Models;

namespace Payment.Domain.Services.Vendedores;

public class AtualizarVendedorService : IAtualizarVendedorService
{
    private readonly IVendedorRepository vendedorRepository;

    public AtualizarVendedorService(
        IVendedorRepository vendedorRepository)
    {
        this.vendedorRepository = vendedorRepository;
    }

    public async Task<Response> Execute(
        AtualizarVendedorRequest request)
    {
        Vendedor? vendedor = await vendedorRepository.ObterModelPorIdAsync(request.Id);

        if (vendedor is null)
        {
            return new(ResponseStatus.NotFound, "Vendedor não encontrado para o Id informado.");
        }

        vendedor.Atualizar(
            request.Nome,
            request.Cpf,
            request.Email,
            request.Telefone);

        return await vendedorRepository.AtualizarAsync(vendedor);
    }
}
