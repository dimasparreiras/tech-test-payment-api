﻿using Payment.Application.Abstraction.Enums;
using Payment.Application.Abstraction.Interfaces.Repositories;
using Payment.Application.Abstraction.Interfaces.Services.Produto;
using Payment.Application.Abstraction.Responses;
using Payment.Application.Requests;
using Payment.Domain.Model.Models;

namespace Payment.Domain.Services.Produtos;

public class AtualizarProdutoService : IAtualizarProdutoService
{
    private readonly IProdutoRepository produtoRepository;

    public AtualizarProdutoService(
        IProdutoRepository produtoRepository)
    {
        this.produtoRepository = produtoRepository;
    }

    public async Task<Response> Execute(
        AtualizarProdutoRequest request)
    {
        Produto? produto = await produtoRepository.ObterModelPorIdAsync(request.Id);

        if (produto is null)
        {
            return new(ResponseStatus.NotFound, "Produto não encontrado para o Id informado.");
        }

        produto.Atualizar(
            request.Descricao,
            request.Valor);

        return await produtoRepository.AtualizarAsync(produto);
    }
}
