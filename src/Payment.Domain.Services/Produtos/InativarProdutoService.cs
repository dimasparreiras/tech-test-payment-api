﻿using Payment.Application.Abstraction.Enums;
using Payment.Application.Abstraction.Interfaces.Repositories;
using Payment.Application.Abstraction.Interfaces.Services.Produto;
using Payment.Application.Abstraction.Responses;
using Payment.Domain.Model.Models;

namespace Payment.Domain.Services.Produtos;

public class InativarProdutoService : IInativarProdutoService
{
    private readonly IProdutoRepository produtoRepository;

    public InativarProdutoService(
        IProdutoRepository produtoRepository)
    {
        this.produtoRepository = produtoRepository;
    }

    public async Task<Response> Execute(
        int request)
    {
        Produto? produto = await produtoRepository.ObterModelPorIdAsync(request);

        if (produto is null)
        {
            return new(ResponseStatus.NotFound, "Produto não encontrado para o Id informado.");
        }

        produto.Inativar();

        return await produtoRepository.AtualizarAsync(produto);
    }
}
