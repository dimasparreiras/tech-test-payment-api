﻿using Payment.Application.Abstraction.Interfaces.Repositories;
using Payment.Application.Abstraction.Interfaces.Services.Produto;
using Payment.Application.Abstraction.Responses;
using Payment.Application.Requests;
using Payment.Domain.Model.Models;

namespace Payment.Domain.Services.Produtos;

public class CadastrarProdutoService : ICadastrarProdutoService
{
    private readonly IProdutoRepository produtoRepository;

    public CadastrarProdutoService(
        IProdutoRepository produtoRepository)
    {
        this.produtoRepository = produtoRepository;
    }

    public async Task<Response> Execute(
        CadastrarProdutoRequest request)
    {
        int id = await produtoRepository.ObterProximoId();

        Produto produto = new Produto(
            id,
            request.Descricao,
            request.Valor);

       return await produtoRepository.AdicionarAsync(produto);
    }
}
