﻿using Payment.Application.Abstraction.Enums;
using Payment.Application.Abstraction.Interfaces.Repositories;
using Payment.Application.Abstraction.Interfaces.Services.Pedido;
using Payment.Application.Abstraction.Responses;
using Payment.Domain.Model.Enums;
using Payment.Domain.Model.Models;

namespace Payment.Domain.Services.Pedidos;

public class InformarCancelamentoService : IInformarCancelamentoService
{
    private readonly IPedidoRepository pedidoRepository;

    public InformarCancelamentoService(
        IPedidoRepository pedidoRepository)
    {
        this.pedidoRepository = pedidoRepository;
    }

    public async Task<Response> Execute(
        int request)
    {
        Pedido? pedido = await pedidoRepository.ObterModelPorIdAsync(request);
        if (pedido is null)
        {
            return new Response(ResponseStatus.NotFound, "Não foi encontrado pedido para o Id informado.");
        }

        StatusPedido status = (StatusPedido)pedido.Status;
        if (status == StatusPedido.AguardandoPagamento ||
            status == StatusPedido.PagamentoAprovado)
        {
            pedido.AtualizarStatus(StatusPedido.Cancelado);
            return await pedidoRepository.AtualizarAsync(pedido);
        }

        return new Response(ResponseStatus.BadRequest, "O status atual do pedido não permite mudança para Cancelado.");
    }
}
