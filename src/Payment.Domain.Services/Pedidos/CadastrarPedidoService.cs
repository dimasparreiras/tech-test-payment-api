﻿using Payment.Application.Abstraction.Enums;
using Payment.Application.Abstraction.Interfaces.Repositories;
using Payment.Application.Abstraction.Interfaces.Services.Venda;
using Payment.Application.Abstraction.Responses;
using Payment.Application.Requests;
using Payment.Domain.Model.Models;

namespace Payment.Domain.Services.Pedidos;

public class CadastrarPedidoService : ICadastrarPedidoService
{
    private readonly IVendedorRepository vendedorRepository;
    private readonly IProdutoRepository produtoRepository;
    private readonly IPedidoRepository pedidoRepository;
    private readonly IProdutoVendidoRepository produtoVendidoRepository;

    public CadastrarPedidoService(
        IVendedorRepository vendedorRepository,
        IProdutoRepository produtoRepository,
        IPedidoRepository pedidoRepository,
        IProdutoVendidoRepository produtoVendidoRepository)
    {
        this.vendedorRepository = vendedorRepository;
        this.produtoRepository = produtoRepository;
        this.pedidoRepository = pedidoRepository;
        this.produtoVendidoRepository = produtoVendidoRepository;
    }

    public async Task<Response> Execute(CadastrarPedidoRequest request)
    {
        if (!request.Produtos.Any() ||
            request.Produtos.Any(p => p.Quantidade <=0 ))
        {
            return new Response(ResponseStatus.BadRequest, "É necessário informar pelo menos um item adquirido.");
        }

        Vendedor? vendedor = await vendedorRepository.ObterModelPorIdAsync(request.IdVendedor);
        if (vendedor is null )
        {
            return new Response(ResponseStatus.NotFound, "Não foi encontrado vendedor para o Id informado.");
        }

        int[] ids = request.Produtos
            .Select(p => p.IdProduto)
            .Distinct()
            .ToArray();

        IEnumerable<Produto> produtos = await produtoRepository.ObterModelPorIdAsync(ids);

        bool produtosNaoEncontrados = ids
            .Except(produtos.Select(p => p.Id).Distinct())
            .Any();

        if (produtosNaoEncontrados)
        {
            return new Response(ResponseStatus.NotFound, "Não foram encontrados todos os produtos informados.");
        }

        Pedido pedido = await CriarPedido(
            request,
            vendedor,
            produtos);

        return await pedidoRepository.AdicionarAsync(pedido);
    }

    internal async Task<Pedido> CriarPedido(
        CadastrarPedidoRequest request,
        Vendedor vendedor,
        IEnumerable<Produto> produtos)
    {
        int idProduto = await pedidoRepository.ObterProximoId();
        int idProdutoVendido = await produtoVendidoRepository.ObterProximoId();

        IList<ProdutoVendido> produtosVendidos = [];
        foreach (Produto produto in produtos)
        {
            int quantidade = request.Produtos
                .Where(p => p.IdProduto == produto.Id)
                .Sum(p => p.Quantidade);

            ProdutoVendido produtoVendido = new ProdutoVendido(idProdutoVendido, produto, quantidade);
            produtosVendidos.Add(produtoVendido);

            idProdutoVendido = idProdutoVendido++;
        }

        return new(idProduto, vendedor, produtosVendidos);
    }
}
