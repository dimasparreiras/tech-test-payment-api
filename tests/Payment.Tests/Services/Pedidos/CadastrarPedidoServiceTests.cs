﻿using FluentAssertions;
using Moq;
using Payment.Application.Abstraction.Enums;
using Payment.Application.Abstraction.Interfaces.Repositories;
using Payment.Application.Abstraction.Responses;
using Payment.Application.Requests;
using Payment.Domain.Model.Models;
using Payment.Domain.Services.Pedidos;

namespace Payment.Tests.Services.Pedidos;

public class CadastrarPedidoServiceTests
{
    private readonly Mock<IVendedorRepository> vendedorRepository;
    private readonly Mock<IProdutoRepository> produtoRepository;
    private readonly Mock<IPedidoRepository> pedidoRepository;
    private readonly Mock<IProdutoVendidoRepository> produtoVendidoRepository;

    public CadastrarPedidoServiceTests()
    {
        vendedorRepository = new Mock<IVendedorRepository>();
        produtoRepository = new Mock<IProdutoRepository>();
        pedidoRepository = new Mock<IPedidoRepository>();
        produtoVendidoRepository = new Mock<IProdutoVendidoRepository>();
    }

    [Fact(DisplayName = "Ao executar o método Execute, retorna uma mensagem de erro quando o Request não tiver produtos.")]
    public async Task Execute_RequestSemProdutos_RetornaMensagemErro()
    {
        // Arrange
        CadastrarPedidoRequest request = new()
        {
            IdVendedor = 1,
            Produtos = []
        };

        CadastrarPedidoService service = new(vendedorRepository.Object, produtoRepository.Object, pedidoRepository.Object, produtoVendidoRepository.Object);

        // Act
        Response actual = await service.Execute(request);

        // Assert
        actual.Status.Should().Be(ResponseStatus.BadRequest);
        actual.Message.Should().Be("É necessário informar pelo menos um item adquirido.");
    }

    [Fact(DisplayName = "Ao executar o método Execute, retorna uma mensagem de erro quando o Request tiver produtos zerados.")]
    public async Task Execute_RequestProdutosZerados_RetornaMensagemErro()
    {
        // Arrange
        CadastrarPedidoRequest request = new()
        {
            IdVendedor = 1,
            Produtos = 
            [
                new()
                {
                    IdProduto = 1,
                    Quantidade = 0
                }
            ]
        };

        CadastrarPedidoService service = new(vendedorRepository.Object, produtoRepository.Object, pedidoRepository.Object, produtoVendidoRepository.Object);

        // Act
        Response actual = await service.Execute(request);

        // Assert
        actual.Status.Should().Be(ResponseStatus.BadRequest);
        actual.Message.Should().Be("É necessário informar pelo menos um item adquirido.");
    }

    [Fact(DisplayName = "Ao executar o método Execute, retorna uma mensagem de erro quando o vendedor não for encontrado.")]
    public async Task Execute_VendedorNaoEncontrado_RetornaMensagemErro()
    {
        // Arrange
        CadastrarPedidoRequest request = new()
        {
            IdVendedor = 1,
            Produtos =
            [
                new()
                {
                    IdProduto = 1,
                    Quantidade = 1
                }
            ]
        };

        CadastrarPedidoService service = new(vendedorRepository.Object, produtoRepository.Object, pedidoRepository.Object, produtoVendidoRepository.Object);

        // Act
        Response actual = await service.Execute(request);

        // Assert
        actual.Status.Should().Be(ResponseStatus.NotFound);
        actual.Message.Should().Be("Não foi encontrado vendedor para o Id informado.");
    }

    [Fact(DisplayName = "Ao executar o método Execute, retorna uma mensagem de erro quando algum produto não for encontrado.")]
    public async Task Execute_ProdutoNaoEncontrado_RetornaMensagemErro()
    {
        // Arrange
        CadastrarPedidoRequest request = new()
        {
            IdVendedor = 1,
            Produtos =
            [
                new()
                {
                    IdProduto = 1,
                    Quantidade = 1
                },

                new()
                {
                    IdProduto = 2,
                    Quantidade = 2
                }
            ]
        };

        vendedorRepository
            .Setup(x => x.ObterModelPorIdAsync(It.IsAny<int>()))
            .ReturnsAsync(new Vendedor(1, "Dimas", "111.222.333-44", "dimasparreiras@gmail.com", "37999851327"));

        produtoRepository
            .Setup(x => x.ObterModelPorIdAsync(It.IsAny<int[]>()))
            .ReturnsAsync([new Produto(1, "Arroz", 5.9m)]);

        CadastrarPedidoService service = new(vendedorRepository.Object, produtoRepository.Object, pedidoRepository.Object, produtoVendidoRepository.Object);

        // Act
        Response actual = await service.Execute(request);

        // Assert
        actual.Status.Should().Be(ResponseStatus.NotFound);
        actual.Message.Should().Be("Não foram encontrados todos os produtos informados.");
    }

    [Fact(DisplayName = "Ao executar o método Execute, retorna uma mensagem de erro quando ocorrer falha ao salvar")]
    public async Task Execute_FalhaAoSalvar_RetornaMensagemErro()
    {
        // Arrange
        CadastrarPedidoRequest request = new()
        {
            IdVendedor = 1,
            Produtos =
            [
                new()
                {
                    IdProduto = 1,
                    Quantidade = 1
                }
            ]
        };

        vendedorRepository
            .Setup(x => x.ObterModelPorIdAsync(It.IsAny<int>()))
            .ReturnsAsync(new Vendedor(1, "Dimas", "111.222.333-44", "dimasparreiras@gmail.com", "37999851327"));

        produtoRepository
            .Setup(x => x.ObterModelPorIdAsync(It.IsAny<int[]>()))
            .ReturnsAsync([new Produto(1, "Arroz", 5.9m)]);

        pedidoRepository
            .Setup(x => x.AdicionarAsync(It.IsAny<Pedido>()))
            .ReturnsAsync(new Response(ResponseStatus.InternalServerError, "Falha ao salvar o pedido."));

        CadastrarPedidoService service = new(vendedorRepository.Object, produtoRepository.Object, pedidoRepository.Object, produtoVendidoRepository.Object);

        // Act
        Response actual = await service.Execute(request);

        // Assert
        actual.Status.Should().Be(ResponseStatus.InternalServerError);
        actual.Message.Should().Be("Falha ao salvar o pedido.");
    }

    [Fact(DisplayName = "Ao executar o método Execute, retorna uma mensagem de erro quando ocorrer sucesso ao salvar")]
    public async Task Execute_SucessoAoSalvar_RetornaOk()
    {
        // Arrange
        CadastrarPedidoRequest request = new()
        {
            IdVendedor = 1,
            Produtos =
            [
                new()
                {
                    IdProduto = 1,
                    Quantidade = 1
                }
            ]
        };

        vendedorRepository
            .Setup(x => x.ObterModelPorIdAsync(It.IsAny<int>()))
            .ReturnsAsync(new Vendedor(1, "Dimas", "111.222.333-44", "dimasparreiras@gmail.com", "37999851327"));

        produtoRepository
            .Setup(x => x.ObterModelPorIdAsync(It.IsAny<int[]>()))
            .ReturnsAsync([new Produto(1, "Arroz", 5.9m)]);

        pedidoRepository
            .Setup(x => x.AdicionarAsync(It.IsAny<Pedido>()))
            .ReturnsAsync(new Response());

        CadastrarPedidoService service = new(vendedorRepository.Object, produtoRepository.Object, pedidoRepository.Object, produtoVendidoRepository.Object);

        // Act
        Response actual = await service.Execute(request);

        // Assert
        actual.Status.Should().Be(ResponseStatus.Ok);
        actual.Message.Should().BeNull();
    }
}
