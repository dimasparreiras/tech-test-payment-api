﻿using FluentAssertions;
using Moq;
using Payment.Application.Abstraction.Enums;
using Payment.Application.Abstraction.Interfaces.Repositories;
using Payment.Application.Abstraction.Responses;
using Payment.Domain.Model.Enums;
using Payment.Domain.Model.Models;
using Payment.Domain.Services.Pedidos;

namespace Payment.Tests.Services.Pedidos;

public class InformarCancelamentoServiceTests
{
    private readonly Mock<IPedidoRepository> pedidoRepository;

    public InformarCancelamentoServiceTests()
    {
        pedidoRepository = new Mock<IPedidoRepository>();
    }

    [Fact(DisplayName = "Ao executar o método Execute, retorna uma mensagem de erro quando o pedido não for encontrado.")]
    public async Task Execute_PedidoNaoEncontrado_RetornaMensagemErro()
    {
        // Arrange
        InformarCancelamentoService service = new(pedidoRepository.Object);

        // Act
        Response actual = await service.Execute(1);

        // Assert
        actual.Status.Should().Be(ResponseStatus.NotFound);
        actual.Message.Should().Be("Não foi encontrado pedido para o Id informado.");
    }

    [Theory(DisplayName = "Ao executar o método Execute, retorna uma mensagem de erro quando o Status atual não permitir mudança.")]
    [InlineData(StatusPedido.EnviadoTransportadora)]
    [InlineData(StatusPedido.Entregue)]
    [InlineData(StatusPedido.Cancelado)]
    public async Task Execute_StatusInvalido_RetornaMensagemErro(
        StatusPedido status)
    {
        // Arrange
        Vendedor vendedor = new Vendedor(1, "Dimas", "111.222.333-44", "dimasparreiras@gmail.com", "37999851327");
        Pedido pedido = new(1, vendedor, []);
        pedido.Status = (int)status;

        pedidoRepository
            .Setup(x => x.ObterModelPorIdAsync(It.IsAny<int>()))
            .ReturnsAsync(pedido);

        InformarCancelamentoService service = new(pedidoRepository.Object);

        // Act
        Response actual = await service.Execute(1);

        // Assert
        pedido.Status.Should().Be((int)status);
        actual.Status.Should().Be(ResponseStatus.BadRequest);
        actual.Message.Should().Be("O status atual do pedido não permite mudança para Cancelado.");
    }

    [Theory(DisplayName = "Ao executar o método Execute, retorna uma mensagem de erro quando ocorrer falha ao atualizar.")]
    [InlineData(StatusPedido.AguardandoPagamento)]
    [InlineData(StatusPedido.PagamentoAprovado)]
    public async Task Execute_FalhaAoAtualizar_RetornaOk(
        StatusPedido status)
    {
        // Arrange
        Vendedor vendedor = new Vendedor(1, "Dimas", "111.222.333-44", "dimasparreiras@gmail.com", "37999851327");
        Pedido pedido = new(1, vendedor, []);
        pedido.Status = (int)status;

        pedidoRepository
            .Setup(x => x.ObterModelPorIdAsync(It.IsAny<int>()))
            .ReturnsAsync(pedido);

        pedidoRepository
            .Setup(x => x.AtualizarAsync(It.IsAny<Pedido>()))
            .ReturnsAsync(new Response(ResponseStatus.InternalServerError, "Falha ao atualizar o status do pedido."));

        InformarCancelamentoService service = new(pedidoRepository.Object);

        // Act
        Response actual = await service.Execute(1);

        // Assert
        pedido.Status.Should().Be((int)StatusPedido.Cancelado);
        actual.Status.Should().Be(ResponseStatus.InternalServerError);
        actual.Message.Should().Be("Falha ao atualizar o status do pedido.");
    }

    [Theory(DisplayName = "Ao executar o método Execute, retorna um Ok quando tiver sucesso.")]
    [InlineData(StatusPedido.AguardandoPagamento)]
    [InlineData(StatusPedido.PagamentoAprovado)]
    public async Task Execute_SucessoAoAtualizar_RetornaOk(
        StatusPedido status)
    {
        // Arrange
        Vendedor vendedor = new Vendedor(1, "Dimas", "111.222.333-44", "dimasparreiras@gmail.com", "37999851327");
        Pedido pedido = new(1, vendedor, []);
        pedido.Status = (int)status;

        pedidoRepository
            .Setup(x => x.ObterModelPorIdAsync(It.IsAny<int>()))
            .ReturnsAsync(pedido);

        pedidoRepository
            .Setup(x => x.AtualizarAsync(It.IsAny<Pedido>()))
            .ReturnsAsync(new Response());

        InformarCancelamentoService service = new(pedidoRepository.Object);

        // Act
        Response actual = await service.Execute(1);

        // Assert
        pedido.Status.Should().Be((int)StatusPedido.Cancelado);
        actual.Status.Should().Be(ResponseStatus.Ok);
        actual.Message.Should().BeNull();
    }
}
