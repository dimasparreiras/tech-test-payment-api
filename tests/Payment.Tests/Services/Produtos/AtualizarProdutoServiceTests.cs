﻿using FluentAssertions;
using Moq;
using Payment.Application.Abstraction.Enums;
using Payment.Application.Abstraction.Interfaces.Repositories;
using Payment.Application.Abstraction.Responses;
using Payment.Application.Requests;
using Payment.Domain.Model.Models;
using Payment.Domain.Services.Produtos;

namespace Payment.Tests.Services.Produtos;

public class AtualizarProdutoServiceTests
{
    private readonly Mock<IProdutoRepository> produtoRepository;

    public AtualizarProdutoServiceTests()
    {
        produtoRepository = new Mock<IProdutoRepository>();
    }

    [Fact(DisplayName = "Ao executar o método Execute, retorna uma mensagem de erro quando não encontrar o produto.")]
    public async Task Execute_ProdutoNaoEncontrado_RetornaMensagemErro()
    {
        // Arrange
        AtualizarProdutoRequest request = new();

        AtualizarProdutoService service = new(produtoRepository.Object);

        // Act
        Response actual = await service.Execute(request);

        // Assert
        actual.Status.Should().Be(ResponseStatus.NotFound);
        actual.Message.Should().Be("Produto não encontrado para o Id informado.");
    }

    [Fact(DisplayName = "Ao executar o método Execute, retorna uma mensagem de erro quando ocorrer falha ao salvar.")]
    public async Task Execute_FalhaAoSalvar_RetornaMensagemErro()
    {
        // Arrange
        AtualizarProdutoRequest request = new();

        produtoRepository
            .Setup(x => x.ObterModelPorIdAsync(It.IsAny<int>()))
            .ReturnsAsync(new Produto(1, "Arroz", 5.9m));

        produtoRepository
            .Setup(x => x.AtualizarAsync(It.IsAny<Produto>()))
            .ReturnsAsync(new Response(ResponseStatus.InternalServerError, "Falha ao salvar o produto."));

        AtualizarProdutoService service = new(produtoRepository.Object);

        // Act
        Response actual = await service.Execute(request);

        // Assert
        actual.Status.Should().Be(ResponseStatus.InternalServerError);
        actual.Message.Should().Be("Falha ao salvar o produto.");
    }

    [Fact(DisplayName = "Ao executar o método Execute, retorna Status Ok quando ocorrer sucesso.")]
    public async Task Execute_Sucesso_RetornaStatusOk()
    {
        // Arrange
        AtualizarProdutoRequest request = new();

        produtoRepository
            .Setup(x => x.ObterModelPorIdAsync(It.IsAny<int>()))
            .ReturnsAsync(new Produto(1, "Arroz", 5.9m));

        produtoRepository
            .Setup(x => x.AtualizarAsync(It.IsAny<Produto>()))
            .ReturnsAsync(new Response());

        AtualizarProdutoService service = new(produtoRepository.Object);

        // Act
        Response actual = await service.Execute(request);

        // Assert
        actual.Status.Should().Be(ResponseStatus.Ok);
        actual.Message.Should().BeNull();
    }
}
