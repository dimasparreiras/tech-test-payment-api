﻿using FluentAssertions;
using Moq;
using Payment.Application.Abstraction.Enums;
using Payment.Application.Abstraction.Interfaces.Repositories;
using Payment.Application.Abstraction.Responses;
using Payment.Application.Requests;
using Payment.Domain.Model.Models;
using Payment.Domain.Services.Produtos;

namespace Payment.Tests.Services.Produtos;

public class CadastrarProdutoServiceTests
{
    private readonly Mock<IProdutoRepository> produtoRepository;

    public CadastrarProdutoServiceTests()
    {
        produtoRepository = new Mock<IProdutoRepository>();
    }

    [Fact(DisplayName = "Ao executar o método Execute, retorna uma mensagem de erro quando ocorrer falha ao salvar.")]
    public async Task Execute_FalhaAoSalvar_RetornaMensagemErro()
    {
        // Arrange
        CadastrarProdutoRequest request = new();

        produtoRepository
            .Setup(x => x.ObterProximoId())
            .ReturnsAsync(1);

        produtoRepository
            .Setup(x => x.AdicionarAsync(It.IsAny<Produto>()))
            .ReturnsAsync(new Response(ResponseStatus.InternalServerError, "Falha ao salvar o produto."));

        CadastrarProdutoService service = new(produtoRepository.Object);

        // Act
        Response actual = await service.Execute(request);

        // Assert
        actual.Status.Should().Be(ResponseStatus.InternalServerError);
        actual.Message.Should().Be("Falha ao salvar o produto.");
    }

    [Fact(DisplayName = "Ao executar o método Execute, retorna Status Ok quando ocorrer sucesso.")]
    public async Task Execute_Sucesso_RetornaStatusOk()
    {
        // Arrange
        CadastrarProdutoRequest request = new();

        produtoRepository
            .Setup(x => x.ObterProximoId())
            .ReturnsAsync(1);

        produtoRepository
            .Setup(x => x.AdicionarAsync(It.IsAny<Produto>()))
            .ReturnsAsync(new Response());

        CadastrarProdutoService service = new(produtoRepository.Object);

        // Act
        Response actual = await service.Execute(request);

        // Assert
        actual.Status.Should().Be(ResponseStatus.Ok);
        actual.Message.Should().BeNull();
    }
}
