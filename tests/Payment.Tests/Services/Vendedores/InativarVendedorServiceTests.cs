﻿using FluentAssertions;
using Moq;
using Payment.Application.Abstraction.Enums;
using Payment.Application.Abstraction.Interfaces.Repositories;
using Payment.Application.Abstraction.Responses;
using Payment.Domain.Model.Models;
using Payment.Domain.Services.Vendedores;
using Payment.Infra.Repository.Repositories;

namespace Payment.Tests.Services.Vendedores;

public class InativarVendedorServiceTests
{
    private readonly Mock<IVendedorRepository> vendedorRepository;

    public InativarVendedorServiceTests()
    {
        vendedorRepository = new Mock<IVendedorRepository>();
    }

    [Fact(DisplayName = "Ao executar o método Execute, retorna uma mensagem de erro quando não encontrar o Vendedor.")]
    public async Task Execute_VendedorNaoEncontrado_RetornaMensagemErro()
    {
        // Arrange
        InativarVendedorService service = new(vendedorRepository.Object);

        // Act
        Response actual = await service.Execute(1);

        // Assert
        actual.Status.Should().Be(ResponseStatus.NotFound);
        actual.Message.Should().Be("Vendedor não encontrado para o Id informado.");
    }

    [Fact(DisplayName = "Ao executar o método Execute, retorna uma mensagem de erro quando ocorrer falha ao salvar.")]
    public async Task Execute_FalhaAoSalvar_RetornaMensagemErro()
    {
        // Arrange
        vendedorRepository
            .Setup(x => x.ObterModelPorIdAsync(It.IsAny<int>()))
            .ReturnsAsync(new Vendedor(1, "Dimas", "111.222.333-44", "dimasparreiras@gmail.com", "37999851327"));

        vendedorRepository
            .Setup(x => x.AtualizarAsync(It.IsAny<Vendedor>()))
            .ReturnsAsync(new Response(ResponseStatus.InternalServerError, "Falha ao salvar o Vendedor."));

        InativarVendedorService service = new(vendedorRepository.Object);

        // Act
        Response actual = await service.Execute(1);

        // Assert
        actual.Status.Should().Be(ResponseStatus.InternalServerError);
        actual.Message.Should().Be("Falha ao salvar o Vendedor.");
    }

    [Fact(DisplayName = "Ao executar o método Execute, retorna Status Ok quando ocorrer sucesso.")]
    public async Task Execute_Sucesso_RetornaStatusOk()
    {
        // Arrange
        Vendedor Vendedor = new Vendedor(1, "Dimas", "111.222.333-44", "dimasparreiras@gmail.com", "37999851327");

        vendedorRepository
            .Setup(x => x.ObterModelPorIdAsync(It.IsAny<int>()))
            .ReturnsAsync(Vendedor);

        vendedorRepository
            .Setup(x => x.AtualizarAsync(It.IsAny<Vendedor>()))
            .ReturnsAsync(new Response());

        InativarVendedorService service = new(vendedorRepository.Object);

        // Act
        Response actual = await service.Execute(1);

        // Assert
        Vendedor.Ativo.Should().BeFalse();
        actual.Status.Should().Be(ResponseStatus.Ok);
        actual.Message.Should().BeNull();
    }
}
