﻿using FluentAssertions;
using Moq;
using Payment.Application.Abstraction.Enums;
using Payment.Application.Abstraction.Interfaces.Repositories;
using Payment.Application.Abstraction.Responses;
using Payment.Application.Requests;
using Payment.Domain.Model.Models;
using Payment.Domain.Services.Vendedores;

namespace Payment.Tests.Services.Vendedores;

public class CadastrarVendedorServiceTests
{
    private readonly Mock<IVendedorRepository> VendedorRepository;

    public CadastrarVendedorServiceTests()
    {
        VendedorRepository = new Mock<IVendedorRepository>();
    }

    [Fact(DisplayName = "Ao executar o método Execute, retorna uma mensagem de erro quando ocorrer falha ao salvar.")]
    public async Task Execute_FalhaAoSalvar_RetornaMensagemErro()
    {
        // Arrange
        CadastrarVendedorRequest request = new();

        VendedorRepository
            .Setup(x => x.ObterProximoId())
            .ReturnsAsync(1);

        VendedorRepository
            .Setup(x => x.AdicionarAsync(It.IsAny<Vendedor>()))
            .ReturnsAsync(new Response(ResponseStatus.InternalServerError, "Falha ao salvar o Vendedor."));

        CadastrarVendedorService service = new(VendedorRepository.Object);

        // Act
        Response actual = await service.Execute(request);

        // Assert
        actual.Status.Should().Be(ResponseStatus.InternalServerError);
        actual.Message.Should().Be("Falha ao salvar o Vendedor.");
    }

    [Fact(DisplayName = "Ao executar o método Execute, retorna Status Ok quando ocorrer sucesso.")]
    public async Task Execute_Sucesso_RetornaStatusOk()
    {
        // Arrange
        CadastrarVendedorRequest request = new();

        VendedorRepository
            .Setup(x => x.ObterProximoId())
            .ReturnsAsync(1);

        VendedorRepository
            .Setup(x => x.AdicionarAsync(It.IsAny<Vendedor>()))
            .ReturnsAsync(new Response());

        CadastrarVendedorService service = new(VendedorRepository.Object);

        // Act
        Response actual = await service.Execute(request);

        // Assert
        actual.Status.Should().Be(ResponseStatus.Ok);
        actual.Message.Should().BeNull();
    }
}
